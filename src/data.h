#ifndef DATA_H
#define DATA_H
#pragma once

#include <opencv2/opencv.hpp>

#include <QMutex>
#include <QObject>

struct Distortion_Params{
    float k_1 = 0.0;
    float k_2 = 0.0;
    float k_3 = 0.0;
    float p_1 = 0.0;
    float p_2 = 0.0;

    Distortion_Params(){}

    Distortion_Params(float k_1, float k_2, float k_3, float p_1, float p_2){
        this->k_1 = k_1;
        this->k_2 = k_2;
        this->k_3 = k_3;
        this->p_1 = p_1;
        this->p_2 = p_2;
    }
};

struct Target{
    cv::Point2f position;
    float radius;
    float depth;
    cv::Point2f position_projector;

    Target(const cv::Vec3f& circle, const float& depth){
        position = cv::Point2f(circle[0],circle[1]);
        radius = circle[2];
        this->depth = depth;
    }
};

struct Correspondence{
    cv::Point2f p_proj;
    cv::Point3f q_kin;

    Correspondence(const cv::Point2f& p = cv::Point2f(0.0,0.0), const cv::Point3f& q = cv::Point3f(0.0,0.0,0.0)){
        p_proj = p;
        q_kin = q;
    }

    Correspondence(const cv::Point2f& p, const Target& t){
        p_proj = p;
        q_kin = cv::Point3f(t.position.x,t.position.y,t.depth);
    }
};

///
/// \brief The Data class contains the data that is shared between different threads
///
class Data : public QObject{

    Q_OBJECT

public:
    explicit Data(QObject *parent = nullptr);

    //////////////////    kinect specific data    //////////////////
    bool kinect_available = false;
    bool capture_kinect = false;
    bool detection_mode = false;
    bool paused = false;
    QMutex mutex_kinect;
    u_int32_t timestamp_kinect;
    cv::Mat rgb_image_kinect;
    cv::Mat ir_image_kinect;
    cv::Mat depth_image_kinect;
    ///intrinsics for the Kinect's IR (depth) camera
    cv::Mat_<double> K_ir = cv::Mat::eye(3,3,CV_64F);
    Distortion_Params distortion_ir;

    //////////////////////    projector data    /////////////////////
    int projector_width = 0;
    int projector_height = 0;

    ///////////////////   target creation data    ///////////////////
    bool auto_generated_list = true;
    std::vector<cv::Point2f> target_list;
    cv::Point2f current_target;
    int current_target_idx = -1;

    bool random_target_position = true;

    ////////////////////   target detection data    /////////////////

    cv::Mat detected_target_image;
    std::vector<Target> detected_targets;
    bool waiting_for_decision = false;
    bool validation_mode = false;

    //////////////   hough circle detection parameters    ///////////
    float hough_param_1 = 40.0;
    float hough_param_2 = 25.0;
    float hough_min_rad = 10.0;
    float hough_max_rad = 50.0;

    /////////////////////   calibration data    /////////////////////
    ///intrinsics for the projector
    cv::Mat_<double> K_projector = cv::Mat::eye(3,3,CV_64F);
    ///transformation, i.e. rotation and translation from Kinect frame to projector frame
    cv::Mat_<double> R = cv::Mat::eye(3,3,CV_64F);
    cv::Mat_<double> t = cv::Mat::zeros(3,1,CV_64F);
    ///correspondences between projector crosshair and Kinect 3D positions
    std::vector<Correspondence> correspondences;


    /////////////////////   mapping functions   /////////////////////

    ///
    /// \brief mapToProjector uses K_projector and the extrinsics R and t to map the 3d point p from kinect space into the projector image space
    /// \param p    3d point in kinect space
    /// \param q    resulting 2d point in projector image space
    /// \return
    ///
    bool mapToProjector(const cv::Point3f& p, cv::Point2f* q);

    ///
    /// \brief mapToProjector uses K_projector and the extrinsics R and t to map the 3d point p from kinect space into the projector image space
    /// \param p    3d point in kinect space
    /// \param q    resulting 2d point in projector image space
    /// \return
    ///
    bool mapToProjector(const cv::Mat& p, cv::Mat* q);

    ///
    /// \brief project transforms (x,y,z) into (x/z, y/z, 1)
    /// \param p    3d point to project
    /// \return
    ///
    bool project(cv::Mat* p);

    ///
    /// \brief setProjectorIntrinsics can be used to set the relevant values in K_projector
    /// \param fx   focal length in px
    /// \param fy   focal length in px
    /// \param cx   principal point x
    /// \param cy   principal point y
    ///
    void setProjectorIntrinsics(const double& fx, const double& fy, const double& cx, const double& cy);

    ///
    /// \brief projectTo3DKinect uses the kinect intrinsics to project an depth image point p to 3d space
    /// \param p        image coordinates in the kinect ir/depth image
    /// \param depth    depth value of the kinect depth image at position p
    /// \param q        resulting 3d point in kinect frame
    /// \return
    ///
    bool projectTo3DKinect(const cv::Point2f&p, const float& depth, cv::Point3f* q);

    ///////////////////////////////////    IO    ///////////////////////////////////

    ///
    /// \brief saveCorrespondences
    /// \param path
    ///
    void saveCorrespondences(const QString& path);

    ///
    /// \brief loadCorrespondences
    /// \param path
    ///
    void loadCorrespondences(const QString& path);

    ///
    /// \brief saveCalibration
    /// \param path
    ///
    void saveCalibration(const QString& path);

    ///
    /// \brief loadCalibration
    /// \param path
    ///
    void loadCalibration(const QString& path);

private:

signals:
    ///
    /// \brief log signals a message to be shown in the logger
    /// \param msg      the message to show
    /// \param type     type of message
    ///
    void log(QString msg, int type);
};

#endif // DATA_H
