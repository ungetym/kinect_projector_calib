QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Kinect_Projector_Calib
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS


CONFIG += c++14

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    kinect.cpp \
    logger.cpp \
    data.cpp \
    graphics_view.cpp \
    optimization.cpp \
    image_ops.cpp \
    projector_window.cpp

HEADERS += \
        mainwindow.h \
    kinect.h \
    logger.h \
    data.h \
    graphics_view.h \
    optimization.h \
    image_ops.h \
    projector_window.h

FORMS += \
        mainwindow.ui \
    projector_window.ui


####  link OpenCV  ####

LIBS += -L/usr/lib/x86_64-linux-gnu \
-lopencv_rgbd \
-lopencv_highgui \
-lopencv_imgcodecs \
-lopencv_core \
-lopencv_imgproc \
-lopencv_ccalib \
-lopencv_calib3d \
-lopencv_features2d

####  link libfreenect  ####

LIBS += -L/usr/local/lib \
-lfreenect2

####  include eigen3  ####

INCLUDEPATH += /usr/include/eigen3

####  link ceres and dependencies  ####

LIBS += /usr/local/lib/libceres.a
LIBS += -L/usr/lib/x86_64-linux-gnu \
-lglog \
-lspqr \
-lcholmod \
-lgflags \
-llapack \
-lblas \
-lcxsparse
