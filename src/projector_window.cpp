#include "projector_window.h"
#include "ui_projector_window.h"

#include <QApplication>
#include <QBitmap>
#include <QDesktopWidget>

Projector_Window::Projector_Window(Data* data, QWidget *parent) :
    QMainWindow(parent),
    ui_(new Ui::Projector_Window),
    data_(data)
{
    ui_->setupUi(this);

    item_ = new QGraphicsPixmapItem();
    scene_ = new QGraphicsScene();
    target_ = new QGraphicsItemGroup();
    //create target marker
    QPen pen(QColor(255,0,0));
    float markersize = 20.0;
    QGraphicsLineItem* line_x = new QGraphicsLineItem(-markersize,0.0,markersize,0.0);
    line_x->setPen(pen);
    target_->addToGroup(line_x);
    QGraphicsLineItem* line_y = new QGraphicsLineItem(0.0,-markersize,0.0,markersize);
    line_y->setPen(pen);
    target_->addToGroup(line_y);
    target_->hide();

    //set width and height of projector window
    data_->projector_width = this->centralWidget()->width();
    data_->projector_height = this->centralWidget()->height();

    item_->setPixmap(QPixmap(data_->projector_width,data_->projector_height));
    QRectF view_rect(0, 0, data_->projector_width, data_->projector_height);
    ui_->display->setSceneRect(view_rect);

    scene_->addItem(item_);
    scene_->addItem(target_);
    ui_->display->setScene(scene_);
}

Projector_Window::~Projector_Window(){
    delete ui_;
}


void Projector_Window::setImage(const cv::Mat &image){
    QImage::Format format;
    if(image.type() == CV_8UC1){
        format = QImage::Format_Grayscale8;
    }
    else if(image.type() == CV_8UC3){
        format = QImage::Format_RGB888;
    }
    else if(image.type() == CV_8UC4){
        format = QImage::Format_RGBA8888;
    }
    else{
        return;
    }
    QImage qImage(image.data, image.cols, image.rows, image.step, format);
    item_->setPixmap(QPixmap::fromImage(qImage));
}

void Projector_Window::switchScreen(){
    screen_id_ = (screen_id_+1)%qApp->screens().size();
    this->setGeometry(QApplication::desktop()->screenGeometry(screen_id_));

    //set width and height of projector window
    data_->projector_width = this->centralWidget()->width();
    data_->projector_height = this->centralWidget()->height();
    QPixmap* pixmap = new QPixmap(data_->projector_width,data_->projector_height);
    pixmap->fill(QColor(0,0,0));
    item_->setPixmap(*pixmap);

    QRectF view_rect(0, 0, data_->projector_width, data_->projector_height);
    ui_->display->setSceneRect(view_rect);
}

void Projector_Window::showCurrentTarget(){
    showTarget(data_->current_target);
}

void Projector_Window::showTarget(const cv::Point2f& p){
    if(data_->projector_width == 0){
        //set width and height of projector window
        data_->projector_width = this->centralWidget()->width();
        data_->projector_height = this->centralWidget()->height();
    }
    QPointF scene_pos = ui_->display->mapToScene(QPoint(p.x,p.y));
    target_->setPos(scene_pos);
    target_->show();
}

void Projector_Window::showMultipleTargets(){
    target_->hide();
    //add new markers if detected target list is larger than currently saved target list
    for(size_t i = targets.size(); i < data_->detected_targets.size(); i++){
        QGraphicsItemGroup* new_marker = new QGraphicsItemGroup();
        //create target marker
        QPen pen(QColor(255,0,0));
        float markersize = 20.0;
        QGraphicsLineItem* line_x = new QGraphicsLineItem(-markersize,0.0,markersize,0.0);
        line_x->setPen(pen);
        new_marker->addToGroup(line_x);
        QGraphicsLineItem* line_y = new QGraphicsLineItem(0.0,-markersize,0.0,markersize);
        line_y->setPen(pen);
        new_marker->addToGroup(line_y);

        targets.push_back(new_marker);
        scene_->addItem(new_marker);
    }

    for(size_t i = 0; i < data_->detected_targets.size(); i++){
        targets[i]->show();
        QPointF pos(data_->detected_targets[i].position_projector.x, data_->detected_targets[i].position_projector.y);
        targets[i]->setPos(pos);
    }
    for(size_t i = data_->detected_targets.size(); i < targets.size(); i++){
        targets[i]->hide();
    }
}
