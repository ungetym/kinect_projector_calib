#ifndef OPTIMIZATION_H
#define OPTIMIZATION_H

#include "data.h"

#include <opencv2/opencv.hpp>

#include <QObject>

#define GLOG_NO_ABBREVIATED_SEVERITIES
#include <ceres/cost_function_to_functor.h>
#include <ceres/numeric_diff_cost_function.h>
#include <ceres/sized_cost_function.h>

///
/// \brief The Projection class is used by the cost functor to calculate the residuals for the optimization
///
class Projection{

public:
    Projection(const cv::Point2f& p, const cv::Point3f& q, const cv::Mat& K_kinect_inv){
        p_.at<double>(0,0) = p.x;
        p_.at<double>(1,0) = p.y;
        q_.at<double>(0,0) = q.x;
        q_.at<double>(1,0) = q.y;
        q_.at<double>(2,0) = q.z;
        K_kinect_inv.copyTo(K_kinect_inv_);
    }

    ///
    /// \brief operator () calculates a residual, i.e. the Kinect 3D point q_ is transformed and projected into the projector image plane where its distance to the corresponding p_ is calculated
    /// \param K_projector      K matrix of the projector
    /// \param rod              rotation from Kinect frame to projector frame as rodrigues vector
    /// \param t                translation from Kinect frame to projector frame
    /// \param residual         resulting distance between p_ and the projected point q_
    /// \return
    ///
    bool operator()(const double* K_projector, const double* rod, const double* t, double* residual) const{
        //calculate rotation matrix from rod parameters
        cv::Mat_<double> R_rod = cv::Mat(3,1,CV_64F);
        R_rod << rod[0], rod[1], rod[2];
        cv::Mat rotation;
        cv::Rodrigues(R_rod,rotation);
        cv::Mat_<double> translation = cv::Mat(3,1,CV_64F);
        translation << t[0], t[1], t[2];

        //transform q into projector space and project to projector image plane
        cv::Mat q_3d;
        q_.copyTo(q_3d);
        q_3d.at<double>(2,0) = 1.0;
        cv::Mat q_proj = rotation*K_kinect_inv_*q_3d*q_.at<double>(2,0)+translation;
        q_proj.at<double>(0,0) = (q_proj.at<double>(0,0)*K_projector[0] + q_proj.at<double>(2,0)*K_projector[2])/q_proj.at<double>(2,0);
        q_proj.at<double>(1,0) = (q_proj.at<double>(1,0)*K_projector[1] + q_proj.at<double>(2,0)*K_projector[3])/q_proj.at<double>(2,0);
        q_proj.at<double>(2,0) = 0.0;
        cv::Mat result = (p_-q_proj).t()*(p_-q_proj);
        residual[0] = std::sqrt(result.at<double>(0,0));
        return true;
    }

private:
    //3x1 vector containing the projector crosshair position and z=0
    cv::Mat p_ = cv::Mat::zeros(3,1,CV_64F);
    //3x1 vector containing the position of the projected crosshair as detected in the Kinect depth image, i.e. (x,y,z) = (x-coordinate in kinect img, y-coordinate in kinect img, depth value)
    cv::Mat q_ = cv::Mat::zeros(3,1,CV_64F);
    //intrinsics of kinect
    cv::Mat K_kinect_inv_;
};



///
/// \brief The CostFunctor class
///
class CostFunctor {
public:
    CostFunctor(const cv::Point2f& p, const cv::Point3f& q, const cv::Mat& K_kinect_inv) : projection_(new ceres::NumericDiffCostFunction<Projection, ceres::CENTRAL, 1, 4, 3, 3>(new Projection(p,q, K_kinect_inv))){
    }

    template <typename T>
    bool operator()(const T* K_projector, const T* rod, const T* t, T* residual) const {
        return projection_(K_projector,rod,t,residual);
    }

private:
    ceres::CostFunctionToFunctor<1,4,3,3> projection_;
};


///
/// \brief The Optimization class
///
class Optimization : public QObject
{
    Q_OBJECT

public:
    Optimization();

public slots:
    ///
    /// \brief startOptimization uses Ceres to find the projector intrinsics and extrinsics based on the correspondences
    /// \param correspondences      between projector crosshair and Kinect 3D positions
    /// \param K_kinect             intrinsics for the kinect
    /// \param K_projector          intrinsics for the projector
    /// \param R                    rotation between Kinect and projector
    /// \param t                    translation between Kinect and projector
    /// \return                     true if successful, false if optimization failed
    ///
    bool startOptimization(const std::vector<Correspondence>& correspondences, cv::Mat_<double>* K_kinect, cv::Mat_<double>* K_projector, cv::Mat* R, cv::Mat* t);

signals:
    void log(QString msg, int type);
};

#endif // OPTIMIZATION_H
