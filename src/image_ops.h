#pragma once

#include "data.h"

#include <opencv2/opencv.hpp>

#include <QObject>

class Image_Ops : public QObject{

    Q_OBJECT

public:

    ///
    /// \brief Image_Ops
    /// \param data
    ///
    Image_Ops(Data* data);

    ///
    /// \brief createNextTargetImage
    ///
    void createNextTarget();

public slots:

    ///
    /// \brief createTargetList
    /// \param num_targets
    ///
    void createTargetList(const int& num_targets);

    ///
    /// \brief findTarget searches for the 'depth-checker' target
    /// \param image            depth image to search the target in
    ///
    void findTarget(const cv::Mat& image);

private:
    /// class containing all current image and property data
    Data* data_;

    ///
    /// \brief checkAndRefineCheckerCorner checks if a point p represents a checkerboard corner and refines it by calculating symmetries in a neighborhood around p
    /// \param p            checkerboard corner candidate
    /// \param refined_p    returns the refined corner if p is a checkerboard corner
    /// \param image        current camera image
    /// \param size         the neighborhood of a corner p is a (2size+1)x(2size+1) window around p
    /// \return             true if p is a checkerboard corner
    ///
    bool checkAndRefineCheckerCorner(const cv::Mat &image, cv::Point2f *p, const int size = 10, const int refine_area = 3);
    bool checkAndRefineCheckerCorner(const cv::Mat &image, cv::Point *p, const int size = 10, const int refine_area = 3);

signals:

    void newTargetCalculated();
    void targetsDetected();
    void correctedNumTargets(const int& num_targets);
};
