#pragma once

#include "data.h"

#include <opencv2/opencv.hpp>

#include <QGraphicsItemGroup>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include <QMainWindow>

namespace Ui {
class Projector_Window;
}

class Projector_Window : public QMainWindow
{
    Q_OBJECT

public:
    explicit Projector_Window(Data* data, QWidget *parent = nullptr);
    ~Projector_Window();

    ///
    /// \brief setImage
    /// \param image
    ///
    void setImage(const cv::Mat& image);

    ///
    /// \brief switchScreen
    ///
    void switchScreen();

public slots:
    ///
    /// \brief showCurrentTarget
    ///
    void showCurrentTarget();

    ///
    /// \brief showMultipleTargets
    ///
    void showMultipleTargets();

private:
    Ui::Projector_Window* ui_;
    Data* data_;

    QGraphicsScene* scene_;
    QGraphicsPixmapItem* item_;
    QGraphicsItemGroup* target_;
    std::vector<QGraphicsItemGroup*> targets;

    int screen_id_ = 0;

    ///
    /// \brief showTarget
    /// \param p
    ///
    void showTarget(const cv::Point2f& p);
};
