#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QApplication>
#include <QDebug>
#include <QFileDialog>
#include <QtGlobal>
#include <QWindow>

using namespace std;
using namespace cv;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui_(new Ui::MainWindow),
    projector_window_(&data_),
    kinect_(&data_),
    image_ops_(&data_)
{
    ui_->setupUi(this);
    this->setWindowTitle("Projector Kinect Calibration");
    ui_->groupbox_manual_target->hide();

    ui_->groupbox_projector_window->setEnabled(false);
    ui_->groupbox_target_setup->setEnabled(false);
    ui_->groupbox_correspondence_detection->setEnabled(false);
    ui_->groupbox_optimization->setEnabled(false);

    ui_->button_accept->setEnabled(false);
    ui_->button_try_again->setEnabled(false);
    ui_->button_reject->setEnabled(false);

    data_.moveToThread(&thread_data_);
    kinect_.moveToThread(&thread_kinect_);
    image_ops_.moveToThread(&thread_img_ops_);
    optimization_.moveToThread(&thread_optimization_);

    //register Mat for signals
    qRegisterMetaType<cv::Mat>("cv::Mat&");
    qRegisterMetaType<cv::Mat>("cv::Mat");

    //connect IO
    connect(ui_->action_load_calibration, &QAction::triggered, this, &MainWindow::buttonClicked);
    connect(ui_->action_save_calibration, &QAction::triggered, this, &MainWindow::buttonClicked);
    connect(ui_->action_load_correspondences, &QAction::triggered, this, &MainWindow::buttonClicked);
    connect(ui_->action_save_correspondences, &QAction::triggered, this, &MainWindow::buttonClicked);
    connect(ui_->action_clear_correspondences, &QAction::triggered, this, &MainWindow::buttonClicked);

    //connect kinect controls
    connect(ui_->button_start, &QPushButton::clicked,  this, &MainWindow::buttonClicked);
    connect(ui_->button_stop, &QPushButton::clicked, this, &MainWindow::buttonClicked);
    connect(ui_->button_pause, &QPushButton::clicked, [this](){data_.paused=!data_.paused;});
    connect(ui_->slider_max_depth, &QSlider::valueChanged, this, &MainWindow::sliderMoved);
    connect(ui_->slider_min_depth, &QSlider::valueChanged, this, &MainWindow::sliderMoved);

    //connect kinect
    connect(&kinect_, &Devices::Kinect::newDepthImage, this, &MainWindow::processDepthImage);
    connect(&kinect_, &Devices::Kinect::log, ui_->logger, &Qt_Tools::Logger::log);

    //connect projector window elements
    connect(ui_->button_open_projector_window, &QPushButton::clicked, this, &MainWindow::buttonClicked);
    connect(ui_->button_close_projector_window, &QPushButton::clicked, this, &MainWindow::buttonClicked);
    connect(ui_->button_switch_screen, &QPushButton::clicked, this, &MainWindow::buttonClicked);

    //connect correspondence creation
    connect(ui_->selector_target_creation_method, qOverload<int>(&QComboBox::activated), [this](int method){
        data_.auto_generated_list = (method==0);
        if(method==0){
            ui_->groupbox_manual_target->hide();ui_->groupbox_list_target->show();
        }
        else{
            ui_->groupbox_manual_target->show();ui_->groupbox_list_target->hide();
        }
    });
    connect(ui_->selector_num_targets, &QSpinBox::editingFinished, [this](){image_ops_.createTargetList(ui_->selector_num_targets->value());});
    connect(ui_->checkbox_random_position, &QCheckBox::clicked,[this](){data_.random_target_position = ui_->checkbox_random_position->isChecked();});
    connect(ui_->button_show_next_target, &QPushButton::clicked, this, &MainWindow::buttonClicked);
    connect(ui_->button_capture_and_detect, &QPushButton::clicked, this, &MainWindow::buttonClicked);
    connect(ui_->button_accept, &QPushButton::clicked, this, &MainWindow::buttonClicked);
    connect(ui_->button_try_again, &QPushButton::clicked, this, &MainWindow::buttonClicked);
    connect(ui_->button_reject, &QPushButton::clicked, this, &MainWindow::buttonClicked);

    //connect image ops
    connect(&image_ops_, &Image_Ops::newTargetCalculated, &projector_window_, &Projector_Window::showCurrentTarget);
    connect(&image_ops_, &Image_Ops::targetsDetected, this, &MainWindow::processDetectedTargets);
    connect(&image_ops_, &Image_Ops::correctedNumTargets, [this](int val){
        ui_->selector_num_targets->blockSignals(true);
        ui_->selector_num_targets->setValue(val);
        ui_->selector_num_targets->blockSignals(false);
    });

    //connect optimization and validation
    connect(ui_->button_start_optimization, &QPushButton::clicked, this, &MainWindow::buttonClicked);
    connect(ui_->button_toggle_validation_mode, &QPushButton::clicked, this, &MainWindow::buttonClicked);

    //connect this signals
    connect(this, &MainWindow::showImage, ui_->graphics_view_kinect, &Qt_Tools::Graphics_View::setCvImage);
    connect(this, &MainWindow::processImage, &image_ops_, &Image_Ops::findTarget);
    connect(this, &MainWindow::showTargets, &projector_window_, &Projector_Window::showMultipleTargets);
    connect(this, &MainWindow::startCapture,  &kinect_, &Devices::Kinect::capture);
    connect(this, &MainWindow::log, ui_->logger, &Qt_Tools::Logger::log);

    //start threads
    thread_data_.start();
    thread_kinect_.start();
    thread_img_ops_.start();
    thread_optimization_.start();

    //search for Kinects
    initKinect();
}

MainWindow::~MainWindow(){
    //close kinect and exit threads
    data_.capture_kinect = false;
    kinect_.close();
    thread_kinect_.exit();
    thread_data_.exit();
    thread_img_ops_.exit();
    thread_optimization_.exit();
    thread_kinect_.wait();
    thread_data_.wait();
    thread_img_ops_.wait();
    thread_optimization_.wait();
    delete ui_;
}

void MainWindow::initKinect(){
    //search for kinects
    int num_kinects = kinect_.findCams();

    if(num_kinects > 0){
        STATUS("Found "+QString::number(num_kinects)+" Kinect camera(s). Starting the first..");
        data_.kinect_available = kinect_.init();
    }
    else{
        STATUS("No Kinects found.");
    }
}

void MainWindow::buttonClicked(){
    const QObject* sender = this->sender();
    if(sender == ui_->action_load_calibration){
        data_.loadCalibration(QFileDialog::getOpenFileName(this,"Select calibration file",".","*.txt"));
        ui_->groupbox_optimization->setEnabled(true);
    }
    else if(sender == ui_->action_save_calibration){
        data_.saveCalibration(QFileDialog::getSaveFileName(this,"Select calibration file destination",".","*.txt"));
    }
    else if(sender == ui_->action_load_correspondences){
        data_.loadCorrespondences(QFileDialog::getOpenFileName(this,"Select correspondences file",".","*.txt"));
        ui_->groupbox_optimization->setEnabled(true);
    }
    else if(sender == ui_->action_save_correspondences){
        data_.saveCorrespondences(QFileDialog::getSaveFileName(this,"Select correspondences file destination",".","*.txt"));
    }
    else if(sender == ui_->action_clear_correspondences){
        data_.correspondences.clear();
    }
    else if(sender == ui_->button_start){
        ui_->groupbox_projector_window->setEnabled(true);
        if(!data_.kinect_available){
            initKinect();
        }
        if(data_.kinect_available){
            emit startCapture();
        }
    }
    else if(sender == ui_->button_stop){
        data_.capture_kinect = false;
        kinect_.stop();
    }
    else if(sender == ui_->button_open_projector_window){
        projector_window_.showFullScreen();
        projector_window_.switchScreen();
        //create auto targets
        image_ops_.createTargetList(ui_->selector_num_targets->value());
        //set manual target selector bounds
        ui_->selector_target_position_x->setMaximum(data_.projector_width);
        ui_->selector_target_position_y->setMaximum(data_.projector_height);

        ui_->groupbox_target_setup->setEnabled(true);

    }
    else if(sender == ui_->button_close_projector_window){
        projector_window_.hide();
    }
    else if(sender == ui_->button_switch_screen){
        if(!projector_window_.isHidden()){
            projector_window_.switchScreen();
            //create auto targets
            image_ops_.createTargetList(ui_->selector_num_targets->value());
            //set manual target selector bounds
            ui_->selector_target_position_x->setMaximum(data_.projector_width);
            ui_->selector_target_position_y->setMaximum(data_.projector_height);
        }
    }
    else if(sender == ui_->button_show_next_target){
        if(!data_.auto_generated_list && !data_.random_target_position){
            data_.current_target = Point2f(ui_->selector_target_position_x->value(),ui_->selector_target_position_y->value());
        }
        image_ops_.createNextTarget();
        ui_->groupbox_correspondence_detection->setEnabled(true);
    }
    else if(sender == ui_->button_capture_and_detect){
        data_.detection_mode = true;

        ui_->groupbox_kinect_controls->setEnabled(false);
        ui_->groupbox_projector_window->setEnabled(false);
        ui_->groupbox_target_setup->setEnabled(false);
        ui_->groupbox_optimization->setEnabled(false);
    }
    else if(sender == ui_->button_accept){
        //save result
        data_.correspondences.push_back(Correspondence(data_.current_target,data_.detected_targets[0]));
        STATUS("Correspondence added. Available: "+QString::number(data_.correspondences.size()));
        //continue depth image streaming
        data_.detection_mode = false;
        data_.waiting_for_decision = false;
        ui_->button_try_again->setEnabled(false);
        ui_->button_accept->setEnabled(false);
        ui_->button_reject->setEnabled(false);

        ui_->groupbox_kinect_controls->setEnabled(true);
        ui_->groupbox_projector_window->setEnabled(true);
        ui_->groupbox_target_setup->setEnabled(true);
        ui_->groupbox_optimization->setEnabled(true);

        if(ui_->checkbox_show_next_on_accept->isChecked()){
            ui_->button_show_next_target->clicked();
        }
    }
    else if(sender == ui_->button_try_again){
        data_.waiting_for_decision = false;
        ui_->button_try_again->setEnabled(false);
        ui_->button_accept->setEnabled(false);
        ui_->button_reject->setEnabled(false);
    }
    else if(sender == ui_->button_reject){
        //continue depth image streaming
        data_.detection_mode = false;
        data_.waiting_for_decision = false;
        ui_->button_try_again->setEnabled(false);
        ui_->button_accept->setEnabled(false);
        ui_->button_reject->setEnabled(false);

        ui_->groupbox_kinect_controls->setEnabled(true);
        ui_->groupbox_projector_window->setEnabled(true);
        ui_->groupbox_target_setup->setEnabled(true);
        ui_->groupbox_optimization->setEnabled(true);
    }
    else if(sender == ui_->button_start_optimization){
        //set initial data for optimization
        data_.K_projector.at<double>(0,0) = 1000.0;
        data_.K_projector.at<double>(1,1) = 1000.0;
        data_.K_projector.at<double>(0,2) = double(data_.projector_width/2);
        data_.K_projector.at<double>(1,2) = double(data_.projector_height/2);
        optimization_.startOptimization(data_.correspondences,&data_.K_ir,&data_.K_projector,&data_.R,&data_.t);
    }
    else if(sender == ui_->button_toggle_validation_mode){
        data_.validation_mode = !data_.validation_mode;
        if(data_.validation_mode){
            ui_->groupbox_projector_window->setEnabled(false);
            ui_->groupbox_target_setup->setEnabled(false);
            ui_->groupbox_correspondence_detection->setEnabled(false);
        }
        else{
            ui_->groupbox_projector_window->setEnabled(true);
            ui_->groupbox_target_setup->setEnabled(true);
            ui_->groupbox_correspondence_detection->setEnabled(true);
        }
    }
}

void MainWindow::sliderMoved(){
    if(this->sender() == ui_->slider_max_depth || this->sender() == ui_->slider_min_depth){
        float max_val = float(ui_->slider_max_depth->value())*2.0/100.0;
        float min_val = float(ui_->slider_min_depth->value())*2.0/100.0;
        kinect_.setDepthRange(min_val,max_val);
        ui_->display_max_depth->setText(QString::number(max_val));
        ui_->display_min_depth->setText(QString::number(min_val));
    }
}

void MainWindow::processDepthImage(const cv::Mat& image){
    if(data_.validation_mode){
        emit showImage(image);
        emit processImage(image);
    }
    else{
        if(!data_.detection_mode){
            emit showImage(image);
        }
        else if(data_.detection_mode && !data_.waiting_for_decision){
            data_.waiting_for_decision = true;
            data_.detected_target_image = image.clone();
            emit processImage(image);
        }
    }
}

void MainWindow::processDetectedTargets(){
    if(data_.validation_mode){
        emit showTargets();
    }
    else{

        if(data_.detected_targets.size() > 1){
            WARN("Multiple targets detected. Please make sure, only one calibration object can be seen by the depth camera.");
        }
        //draw first detected Target and show image
        if(data_.detected_targets.size() > 0){
            cv::circle(data_.detected_target_image,data_.detected_targets[0].position,data_.detected_targets[0].radius,Scalar(255,255,255));
            ui_->button_accept->setEnabled(true);
        }
        else{
            WARN("No target detected.");
        }
        emit showImage(data_.detected_target_image);

        ui_->button_try_again->setEnabled(true);
        ui_->button_reject->setEnabled(true);
    }
}
