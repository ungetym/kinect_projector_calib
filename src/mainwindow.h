#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "data.h"
#include "kinect.h"
#include "image_ops.h"
#include "optimization.h"
#include "projector_window.h"

#include <QMainWindow>
#include <QThread>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow* ui_;
    Projector_Window projector_window_;

    QThread thread_data_;
    Data data_;

    QThread thread_kinect_;
    Devices::Kinect kinect_;

    QThread thread_optimization_;
    Optimization optimization_;

    QThread thread_img_ops_;
    Image_Ops image_ops_;


private slots:
    ///
    /// \brief initKinect searches for kinect devices and initializes the first one
    ///
    void initKinect();

    ///
    /// \brief buttonClicked
    ///
    void buttonClicked();

    ///
    /// \brief sliderMoved
    ///
    void sliderMoved();

    ///
    /// \brief processDepthImage
    /// \param image
    ///
    void processDepthImage(const cv::Mat& image);

    ///
    /// \brief processDetectedTargets
    ///
    void processDetectedTargets();

signals:
    ///
    /// \brief log signals a message to be shown in the logger
    /// \param msg      the message to show
    /// \param type     type of message
    ///
    void log(QString msg, int type);

    ///
    /// \brief showImage
    /// \param image
    ///
    void showImage(const cv::Mat& image);

    ///
    /// \brief processImage
    /// \param image
    ///
    void processImage(const cv::Mat& image);

    ///
    /// \brief showTargets
    ///
    void showTargets();

    ///
    /// \brief startCapture
    ///
    void startCapture();
};

#endif // MAINWINDOW_H
