#include "kinect.h"
#include "logger.h"

#include <libfreenect2/config.h>

#include <QThread>

using namespace std;

Devices::Kinect::Kinect(Data* data){
    this->setObjectName("Kinect");
    data_ = data;
    config = new libfreenect2::Freenect2Device::Config();
    listener_ = new libfreenect2::SyncMultiFrameListener(libfreenect2::Frame::Color|libfreenect2::Frame::Depth);
}

int Devices::Kinect::findCams(){
    //list number of Kinects
    return freenect2_.enumerateDevices();
}

bool Devices::Kinect::init(const int idx){
    STATUS("Initializing Kinect..");
    //close other device if open
    if(dev_!= nullptr){
        dev_->close();
    }

    //open new device
    dev_ = freenect2_.openDevice(idx);

    if(dev_ == nullptr){
        ERROR("Default Kinect device could not be opened.");
        return false;
    }

    //attach frame listener
    dev_->setColorFrameListener(listener_);
    dev_->setIrAndDepthFrameListener(listener_);

    STATUS("Kinect successfully initialized.");

    return true;
}

void Devices::Kinect::capture(){
    if(dev_ == nullptr){
        ERROR("No Kinect available.");
        return;
    }

    //start the device
    if (!dev_->start()){
        ERROR("Kinect could not be started.");
        return;
    }

    //get camera intrinsics for mapping purposes
    libfreenect2::Freenect2Device::IrCameraParams params_ir = dev_->getIrCameraParams();
    //libfreenect2::Freenect2Device::ColorCameraParams params_rgb = dev_->getColorCameraParams();

    data_->distortion_ir = Distortion_Params(params_ir.k1,params_ir.k2,params_ir.k3,params_ir.p1,params_ir.p2);
    vector<float> K_ir = {params_ir.fx, 0.0, params_ir.cx, 0.0, params_ir.fy, params_ir.cy, 0.0, 0.0, 1.0};
    data_->K_ir = cv::Mat(3,3,CV_32F,K_ir.data()).clone();

    //vector<float> K_rgb_data = {params_rgb.fx, 0.0, params_rgb.cx, 0.0, params_rgb.fy, params_rgb.cy, 0.0, 0.0, 1.0};
    //cv::Mat K_rgb(3,3,CV_32F,K_rgb_data.data());
    //K_rgb = K_rgb.inv();
    //data_->K_rgb_inv = K_rgb.clone();

    //start capturing
    STATUS("Start Kinect capturing");

    libfreenect2::Frame* rgb;
    //libfreenect2::Frame* ir;
    libfreenect2::Frame* depth;
    libfreenect2::FrameMap frames;

    data_->capture_kinect = true;
    while(data_->capture_kinect){
        if(data_->paused){
            QThread::msleep(1000);
        }
        else if(data_->mutex_kinect.try_lock()){
            //get new images and save to cv::Mats
            if (!listener_-> waitForNewFrame(frames, 1000)){
                ERROR("Kinect Timeout.");
                return;
            }
            rgb = frames[libfreenect2::Frame::Color];
            //ir = frames[libfreenect2::Frame::Ir];
            depth = frames[libfreenect2::Frame::Depth];

            cv::flip(cv::Mat(depth->height, depth->width, CV_32FC1, depth->data),data_->depth_image_kinect,1);

            data_->timestamp_kinect = rgb->timestamp;

            data_->mutex_kinect.unlock();
            emit newImage(data_->rgb_image_kinect);
            emit newDepthImage(data_->depth_image_kinect);
            listener_->release(frames);
        }
    }
}

void Devices::Kinect::stop(){
    STATUS("Stopping Kinect..");
    if(dev_ != nullptr){
        if(!dev_->stop()){
            ERROR("Stopping the Kinect failed.");
        }
    }
    STATUS("Kinect stopped.");
}

void Devices::Kinect::close(){
    STATUS("Closing Kinect..");
    if(dev_ != nullptr){
        if(!dev_->close()){
            ERROR("Closing the Kinect failed.");
        }
    }
    STATUS("Kinect device successfully closed.");
}

void Devices::Kinect::setDepthRange(const float& min, const float& max){
    if(dev_ != nullptr){
        config->MinDepth = min;
        config->MaxDepth = max;
        dev_->setConfiguration(*config);
    }
}
