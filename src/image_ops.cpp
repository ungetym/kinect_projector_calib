#include "image_ops.h"

using namespace cv;
using namespace std;

Image_Ops::Image_Ops(Data *data){
    data_ = data;
}

void Image_Ops::createTargetList(const int& num_targets){
    //get window size
    int height = data_->projector_height;
    int width = data_->projector_width;
    float ratio = float(width)/float(height);

    int num_height = int(sqrt(float(num_targets)/ratio)+1.0);
    int num_width = int(sqrt(float(num_targets)/ratio)*ratio+1.0);

    if(num_height*num_width != num_targets){
        emit correctedNumTargets(num_height*num_width);
    }

    int height_step = int(float(height)/float(num_height));
    int height_offset = int(0.5*float(height)/float(num_height));

    int width_step = int(float(width)/float(num_width));
    int width_offset = int(0.5*float(width)/float(num_width));

    data_->target_list.clear();
    data_->current_target_idx = -1;

    for(int i = 0; i < num_height; i++){
        for(int j = 0; j < num_width; j++){
            data_->target_list.push_back(Point2f(width_offset+j*width_step,height_offset+i*height_step));
        }
    }
}

void Image_Ops::createNextTarget(){
    if(data_->auto_generated_list){
        if(data_->target_list.size() == 0){
            return;
        }
        else{
            data_->current_target_idx++;
            data_->current_target_idx = data_->current_target_idx % data_->target_list.size();
            data_->current_target = data_->target_list[data_->current_target_idx];
        }
    }
    else{
        if(data_->random_target_position){
            //get window size
            int height = data_->projector_height;
            int width = data_->projector_width;
            //generate random coorindates within pixel range
            int x = std::rand() % width;
            int y = std::rand() % height;
            data_->current_target = Point2f(x,y);
        }
    }

    emit newTargetCalculated();
}

void Image_Ops::findTarget(const Mat& image){
    cv::Mat kinect_img_gray_;
    image.convertTo(kinect_img_gray_, CV_8UC1, 255.0/2000.0, 0);

    std::vector<Vec3f> circles;
    std::vector<float> depths;

    //smooth it, otherwise a lot of false circles may be detected
    GaussianBlur(kinect_img_gray_, kinect_img_gray_, Size(5, 5), 2, 2 );

    //apply the Hough transform to find the circles
    HoughCircles(kinect_img_gray_, circles, CV_HOUGH_GRADIENT, 1, kinect_img_gray_.rows/4, data_->hough_param_1, data_->hough_param_2, data_->hough_min_rad, data_->hough_max_rad);

    for(size_t i = 0; i < circles.size(); i++){
        Vec3f& circle = circles[i];

        //get relevant part of the image
        Range row_range(max(0.0f,circle[1]-circle[2]-5),min(float(image.rows),circle[1]+circle[2]+5));
        Range col_range(max(0.0f,circle[0]-circle[2]-5),min(float(image.cols),circle[0]+circle[2]+5));
        cv::Mat circle_image = kinect_img_gray_(row_range,col_range).clone();

        cv::Point2f mid(circle[0]-col_range.start,circle[1]-row_range.start);

        if(!checkAndRefineCheckerCorner(circle_image,&mid,int(0.5*circle[2]))){
            circles.erase(circles.begin()+i);
            i--;
        }
        else{
            circle = Vec3f(col_range.start+mid.x,row_range.start+mid.y,circle[2]);
            row_range = Range(max(0.0f,circle[1]-0.5f*circle[2]),min(float(image.rows),circle[1]+0.5f*circle[2]));
            col_range = Range(max(0.0f,circle[0]-0.5f*circle[2]),min(float(image.cols),circle[0]+0.5f*circle[2]));
            Mat depth_area = image(row_range,col_range).clone();
            float* data = reinterpret_cast<float*>(depth_area.data);
            for(int idx = 0;idx < depth_area.rows*depth_area.cols; idx++){
                data[idx] = (data[idx] == 0.0) ? 5000.0 : data[idx];
            }
            Mat mask;
            double avg = double(sum(depth_area)[0])/double(depth_area.rows*depth_area.cols);
            threshold(depth_area,mask,avg,8000.0,CV_THRESH_BINARY_INV);
            Mat element = getStructuringElement( MORPH_RECT, Size( 7, 7 ),Point( 0, 0 ) );
            cv::erode(mask,mask,element);
            mask.convertTo(mask,CV_8U);
            Mat masked;
            depth_area.copyTo(masked,mask);
            double depth = double(sum(masked)[0])/(double(sum(mask)[0])/255.0);
            depths.push_back(depth);
        }
    }

    data_->detected_targets.clear();
    for(size_t i = 0; i < depths.size(); i++){
        data_->detected_targets.push_back(Target(circles[i],depths[i]));
        if(data_->validation_mode){
            //calculate corresponding projector position
            data_->mapToProjector(cv::Point3f(circles[i][0],circles[i][1],depths[i]), &data_->detected_targets.back().position_projector);
        }
    }
    emit targetsDetected();
}

bool Image_Ops::checkAndRefineCheckerCorner(const Mat &image, Point* p, const int size, const int refine_area){
    cv::Point2f refined_p_f(p->x,p->y);
    bool success = checkAndRefineCheckerCorner(image,&refined_p_f,size,refine_area);
    if(success){
        *p = cv::Point(int(refined_p_f.x+0.5),int(refined_p_f.y+0.5));
    }
    return success;
}

bool Image_Ops::checkAndRefineCheckerCorner(const Mat &image, Point2f* p, const int size, const int refine_area){
    float scale = 1.0;//max(2.0,12.0/(float)size);
    int size_scaled = (int)scale*size;
    int edge_size = 2*size_scaled+1;//edge size of neighborhood upscaled
    double num_of_px = (double)(edge_size*edge_size);//px in upscaled neighborhood

    //create lookup tables for mirroring
    Mat lut_x = Mat(edge_size,edge_size,CV_8UC1);
    Mat lut_y = Mat(edge_size,edge_size,CV_8UC1);
    for(float row = 0; row < lut_x.rows; row++){
        for(float col = 0; col < lut_x.cols; col++){
            float mirrored_x, mirrored_y;
            if(row<col && row+col<2*size_scaled+2){
                mirrored_y = (float)size_scaled - row;
                mirrored_x = col + (mirrored_y - row) * ((float)size_scaled - col) / ((float)size_scaled - row);
            }
            else if(row>col && row+col>2*size_scaled+1){
                mirrored_y = 3.0*(float)size_scaled + 2.0 - row;
                mirrored_x = col + (mirrored_y - row) * ((float)size_scaled - col) / ((float)size_scaled - row);
            }
            else if(row<col && row+col>=2*size_scaled+1){
                mirrored_x = 3.0*(float)size_scaled + 2.0 - col;
                mirrored_y = row + (mirrored_x - col) * ((float)size_scaled - row) / ((float)size_scaled - col);
            }
            else if(row>col && row+col<=2*size_scaled+1){
                mirrored_x = (float)size_scaled - col;
                mirrored_y = row + (mirrored_x - col) * ((float)size_scaled - row) / ((float)size_scaled - col);
            }
            lut_x.at<uchar>((int)row,(int)col) = (uchar)mirrored_x;
            lut_y.at<uchar>((int)row,(int)col) = (uchar)mirrored_y;
        }
    }


    float best_score_point = 0.0;
    float best_score_ring = 0.0;
    Point2f best_position = *p;

    float score_point_symmetry, score_ring_symmetry;
    Point2f q;

    Mat neighborhood,flipped,mirrored,average;

    auto isValid = [](float a, float b){return (a>0.75)&(b>0.75);};

    //in order to refine the corner position, the symmetry scores are calculated for all points in a refine_areaxrefine_area neighborhood of an initial corner
    for(int i = -refine_area; i < refine_area+1; i++){
        for(int j = -refine_area; j < refine_area+1; j++){
            q = *p + Point2f(i,j);

            //calculate bounding box
            Rect bounding_box(q.x-size,q.y-size,2*size+1,2*size+1);
            if(bounding_box.x < 0 || bounding_box.y < 0 || q.x+2*size+2 > image.cols || q.y+2*size+2 > image.rows){//neighborhood not within image bounds
                continue;
            }

            //binarize neighborhood using the average gray value
            neighborhood = image(bounding_box).clone();

            //cv::resize(neighborhood,neighborhood,Size(0.0,0.0),(int)scale,(int)scale,INTER_NEAREST);
            double avg = (double)(sum(neighborhood)[0])/num_of_px;
            threshold(neighborhood,neighborhood,avg,255,CV_THRESH_BINARY);

            //if the thresholded image does not contain 30-70% black pixels, it cannot represent a checkerboard corner
            double num_of_black_px = (double)(sum(neighborhood)[0])/255.0;
            if(num_of_black_px < 0.3*num_of_px || num_of_black_px > 0.7*num_of_px){
                continue;
            }

            //create mirrored neighborhoods via flip and mirroring at rectangle via lut
            flip(neighborhood,flipped,-1);

            mirrored = Mat(neighborhood.rows,neighborhood.cols,neighborhood.type());
            for(int row = 0; row < mirrored.rows; row++){
                for(int col = 0; col < mirrored.cols; col++){
                    mirrored.at<uchar>(row,col) = neighborhood.at<uchar>(lut_y.at<uchar>(row,col),lut_x.at<uchar>(row,col));
                }
            }

            //compare mirrored/flipped to original neighborhood in order to determine the symmetry
            average = 0.5*(flipped+neighborhood);
            float correct_px = 0;
            float wrong_px = 0;
            for(int px = 0; px<average.rows*average.cols; px++){
                if(average.data[px] == 0 || average.data[px] == 255){
                    correct_px++;
                }
                else{
                    wrong_px++;
                }
            }
            average = 0.5*(mirrored+neighborhood);
            float correct_px_mirror = 0;
            float wrong_px_mirror = 0;
            for(int px = 0; px<average.rows*average.cols; px++){
                if(average.data[px] == 0 || average.data[px] == 255){
                    correct_px_mirror++;
                }
                else{
                    wrong_px_mirror++;
                }
            }
            //calculate scores
            score_point_symmetry = correct_px/(correct_px+wrong_px);
            score_ring_symmetry = correct_px_mirror/(correct_px_mirror+wrong_px_mirror);
            if(isValid(score_point_symmetry,score_ring_symmetry) & !isValid(best_score_point,best_score_ring)){
                best_score_point = score_point_symmetry;
                best_score_ring = score_ring_symmetry;
                best_position = q;
            }
            else if(score_point_symmetry+score_ring_symmetry > best_score_point+best_score_ring){
                best_score_point = score_point_symmetry;
                best_score_ring = score_ring_symmetry;
                best_position = q;
            }
        }
    }


    //high symmetry scores indicate checkerboard corners
    if(!isValid(best_score_point,best_score_ring)){

        return false;
    }

    *p = best_position;
    return true;
}
