#include "data.h"
#include "logger.h"

#include <QFileDialog>
#include <QTextStream>

using namespace cv;
using namespace std;

Data::Data(QObject *parent) : QObject(parent){

}

///////////////////////////////////////     IO functions     ///////////////////////////////////////

void Data::saveCorrespondences(const QString& path){
    if(path.size() == 0){
        STATUS("No file chosen");
        return;
    }

    std::ofstream file(path.toStdString(), std::ofstream::out);
    if(file.is_open()){
        for(const Correspondence& c : correspondences){
            file << c.p_proj.x << " " << c.p_proj.y << " " << c.q_kin.x << " " << c.q_kin.y << " " << c.q_kin.z << "\n";
        }
        file.close();
    }
    else{
        ERROR("Saving correspondences failed. Unable to open file.");
    }
}

void Data::loadCorrespondences(const QString& path){
    if(path.size() == 0){
        STATUS("No file chosen");
        return;
    }

    std::ifstream file(path.toStdString(), std::ifstream::in);

    if(file.is_open()){
        //read all saved points
        while(!file.eof()){
            Correspondence c;
            file >>  c.p_proj.x >> c.p_proj.y >> c.q_kin.x >> c.q_kin.y >> c.q_kin.z;
            if(!file.eof()){
                correspondences.push_back(c);
            }
        }
        file.close();
    }
    else{
        ERROR("Loading correspondences failed. Unable to open file.");
    }
}

void Data::saveCalibration(const QString& path){
    if(path.size() == 0){
        STATUS("No file chosen");
        return;
    }

    QFile file(path);
    if(file.open(QIODevice::WriteOnly)){
        QTextStream stream(&file);
        stream << "Rotation\n";
        for(int i = 0; i < 9; i++){
            stream << R.at<double>(i/3,i%3) << " ";
            if(i%3 == 2){
                stream << "\n";
            }
        }
        stream << "Translation\n";
        for(int i = 0; i < 3; i++){
            stream << t.at<double>(i,0) << "\n";
        }
        stream << "K_projector\n";
        for(int i = 0; i < 9; i++){
            stream << K_projector.at<double>(i/3,i%3) << " ";
            if(i%3 == 2){
                stream << "\n";
            }
        }
        stream << "K_kinect\n";
        for(int i = 0; i < 9; i++){
            stream << K_ir.at<double>(i/3,i%3) << " ";
            if(i%3 == 2){
                stream << "\n";
            }
        }
        file.close();
        STATUS("Calibration successfully saved.");
    }
    else{
        ERROR("Could not create/open file "+path);
    }
}

void Data::loadCalibration(const QString& path){
    if(path.size() == 0){
        STATUS("No file chosen");
        return;
    }

    QFile file(path);
    if(file.open(QIODevice::ReadOnly)){
        QTextStream stream(&file);
        QString line;
        for(int i = 0; i < 4; i++){
            stream >> line;
            if(line.compare("Rotation") == 0){
                for(int i = 0; i < 9; i++){
                    stream >> R.at<double>(i/3,i%3);
                }
            }
            else if(line.compare("Translation") == 0){
                for(int i = 0; i < 9; i++){
                    stream >> t.at<double>(i,0);
                }
            }
            else if(line.compare("K_projector") == 0){
                for(int i = 0; i < 9; i++){
                    stream >> K_projector.at<double>(i/3,i%3);
                }
            }
            else if(line.compare("K_kinect") == 0){
                for(int i = 0; i < 9; i++){
                    stream >> K_ir.at<double>(i/3,i%3);
                }
            }
        }
        file.close();
        STATUS("Calibration successfully loaded.");
    }
    else{
        ERROR("Could not open file "+path);
    }
}

///////////////////////////////////////    projection functions    ///////////////////////////////////////

bool Data::mapToProjector(const cv::Point3f& p, cv::Point2f* q){
    cv::Mat_<double> p_mat = cv::Mat(3,1,CV_64F);
    p_mat << p.x, p.y, p.z;
    cv::Mat q_mat = cv::Mat(3,1,CV_64F);
    if(!mapToProjector(p_mat, &q_mat)){
        return false;
    }
    q->x = q_mat.at<double>(0,0);
    q->y = q_mat.at<double>(1,0);
    return true;
}

bool Data::mapToProjector(const cv::Mat &p, cv::Mat* q){
    if(q->rows != 3 || q->cols != 1){
        return false;
    }
    cv::Mat_<double> p_3d = cv::Mat(3,1,CV_64F);
    p_3d << p.at<double>(0,0), p.at<double>(1,0), 1.0;
    p_3d = K_ir.inv()*p_3d*p.at<double>(2,0);

    *q = K_projector*(R*p_3d+t);
    return project(q);
}

bool Data::project(cv::Mat* p){
    if(p->rows != 3 || p->cols != 1 || p->type() != CV_64F || p->at<double>(2,0) == 0.0){
        return false;
    }

    p->at<double>(0,0) /= p->at<double>(2,0);
    p->at<double>(1,0) /= p->at<double>(2,0);
    p->at<double>(2,0) = 1.0;

    return true;
}

void Data::setProjectorIntrinsics(const double& fx, const double& fy, const double& cx, const double& cy){
    K_projector << fx, 0.0, cx,
            0.0, fy, cy,
            0.0, 0.0, 1.0;
}

bool Data::projectTo3DKinect(const cv::Point2f&p, const float& depth, cv::Point3f* q){
    cv::Mat_<double> p_mat = cv::Mat(3,1,CV_64F);
    p_mat << p.x, p.y, 1.0;

    cv::Mat q_mat = K_projector.inv()*p_mat*depth;
    q->x = q_mat.at<double>(0,0);
    q->y = q_mat.at<double>(1,0);
    q->z = q_mat.at<double>(2,0);

    return true;
}
