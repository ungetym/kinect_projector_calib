#ifndef LOGGER_H
#define LOGGER_H
#pragma once

#include <opencv2/opencv.hpp>

#include <QTextEdit>

//define logger macros for different types of messages
#define ERROR(msg) emit log(msg, Qt_Tools::LOG_ERROR)
#define WARN(msg) emit log(msg, Qt_Tools::LOG_WARNING)
#define STATUS(msg) emit log(msg, Qt_Tools::LOG_NORMAL)
#define LOGMATRIX(msg, mat) emit logMat(msg, mat)

namespace Qt_Tools{

const int LOG_NORMAL = 0;
const int LOG_WARNING = 1;
const int LOG_ERROR = 2;
const int LOG_APPEND = 3;

class Logger : public QTextEdit
{
    Q_OBJECT

public:
    ///
    /// \brief Logger is the standard constructor
    /// \param parent
    ///
    explicit Logger(QWidget *parent = nullptr);

public slots:

    ///
    /// \brief log
    /// \param msg
    /// \param type
    ///
    void log(QString msg, int type);

    ///
    /// \brief logMat
    /// \param msg
    /// \param mat
    ///
    void logMat(QString msg, cv::Mat mat);

};
}
#endif // LOGGER_H
