#include "logger.h"

#include <QTime>

Qt_Tools::Logger::Logger(QWidget *parent) : QTextEdit(parent){
}

void Qt_Tools::Logger::log(QString msg, int type){
    QString new_log;
    if(type==LOG_ERROR){//set error text
        QObject* sender = QObject::sender();
        new_log+=QTime::currentTime().toString()+": "+sender->objectName()+" Error: ";
        setTextColor(QColor::fromRgb(255,0,0));
    }
    else if(type==LOG_NORMAL){//set status text
        new_log+=QTime::currentTime().toString()+": ";
        setTextColor(QColor::fromRgb(0,0,0));
    }
    else if(type==LOG_WARNING){//set warning text
        new_log+=QTime::currentTime().toString()+": Warning: ";
        setTextColor(QColor::fromRgb(255,128,0));
    }
    new_log+=msg;

    if(type==LOG_APPEND){//append msg to last logger entry
        insertPlainText(new_log);
    }
    else{
        append(new_log);
    }
    repaint();
}

void Qt_Tools::Logger::logMat(QString msg, cv::Mat mat){
    setTextColor(QColor::fromRgb(0,0,0));
    append(msg);
    for(int row = 0; row<mat.rows; row++){
        QString mat_row = "  ";
        for(int col = 0; col<mat.cols; col++){
            if(mat.type()==CV_64F){
                mat_row += QString::number((float)mat.at<double>(row,col)).mid(0,8)+"\t";
            }
            else if(mat.type()==CV_32F){
                mat_row += QString::number((float)mat.at<double>(row,col)).mid(0,8)+"\t";
            }
        }
        append(mat_row);
    }
    repaint();
}
