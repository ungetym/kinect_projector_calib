#include "logger.h"
#include "optimization.h"

#include <ceres/problem.h>
#include <ceres/autodiff_cost_function.h>
#include <ceres/cost_function.h>
#include <ceres/solver.h>

using namespace std;
using namespace ceres;
using namespace cv;

Optimization::Optimization(){

}

bool Optimization::startOptimization(const vector<Correspondence>& correspondences, Mat_<double> * K_kinect, Mat_<double> *K_projector, Mat* R, Mat* t){

    //only use fx, fy, cx and cy for optimization
    Mat_<double> K_projector_compact = Mat(4,1,CV_64F);
    K_projector_compact << K_projector->at<double>(0,0), K_projector->at<double>(1,0), K_projector->at<double>(0,2), K_projector->at<double>(1,2);

    //transform rotation matrix to rodrigues vector to reduce the number of parameters
    Mat_<double> R_rodrigues = Mat(3,3,CV_64F);
    Rodrigues(*R,R_rodrigues);

    //calculate inverse of kinect K matrix
    Mat K_kinect_inv = K_kinect->inv();

    //setup ceres
    Problem problem;
    for(const Correspondence& c : correspondences){
        CostFunction* cost_function = new AutoDiffCostFunction<CostFunctor, 1, 4, 3, 3>(new CostFunctor(c.p_proj,c.q_kin, K_kinect_inv));
        problem.AddResidualBlock(cost_function, NULL, K_projector_compact.ptr<double>(), R_rodrigues.ptr<double>(), t->ptr<double>());
    }

    //set solver options
    Solver::Options options;
    options.minimizer_type = ceres::TRUST_REGION;
    options.linear_solver_type = ceres::SPARSE_NORMAL_CHOLESKY;
    options.minimizer_progress_to_stdout = true;
    options.max_num_iterations = 10000;
    Solver::Summary summary;

    //start solver and print summary to terminal
    Solve(options, &problem, &summary);
    cout << summary.BriefReport() << "\n";

    //transform optimized rodrigues vector back to rotation matrix
    Rodrigues(R_rodrigues, *R);

    //save projector intrinsics to K matrix
    *K_projector << K_projector_compact.at<double>(0,0), 0.0, K_projector_compact.at<double>(0,2),
                   0.0, K_projector_compact.at<double>(1,0), K_projector_compact.at<double>(1,2),
                   0.0, 0.0, 1.0;

    //test output
    cerr << *K_projector << "\n" << *R << "\n" << *t;

    return true;
}
