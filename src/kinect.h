#ifndef KINECT_H
#define KINECT_H
#pragma once

#include "data.h"

#include <libfreenect2/frame_listener_impl.h>
#include <libfreenect2/libfreenect2.hpp>

namespace Devices{

class Kinect : public QObject
{
    Q_OBJECT

public:
    Kinect(Data* data);

public slots:
    ///
    /// \brief findCams searches for Kinect devices
    /// \return
    ///
    int findCams();

    ///
    /// \brief init starts the Kinect with specified index
    /// \param idx      index of Kinect to start
    /// \return         true if successful, false if init fails
    ///
    bool init(const int idx = 0);

    ///
    /// \brief capture starts the capturing loop
    ///
    void capture();

    ///
    /// \brief stop the kinect
    ///
    void stop();

    ///
    /// \brief close the Kinect
    ///
    void close();

    ///
    /// \brief setDepthRange
    /// \param min
    /// \param max
    ///
    void setDepthRange(const float& min, const float& max);


private:
    Data* data_ = nullptr;
    int num_cams = 0;
    libfreenect2::Freenect2 freenect2_;
    libfreenect2::Freenect2Device* dev_ = nullptr;
    libfreenect2::PacketPipeline* pipeline_ = nullptr;
    libfreenect2::SyncMultiFrameListener* listener_;
    libfreenect2::Freenect2Device::Config* config = nullptr;


signals:
    void log(QString msg, int type);
    void newImage(cv::Mat& image);
    void newDepthImage(cv::Mat& image);
};
}

#endif // KINECT_H
