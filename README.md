# Kinect-to-Projector Calibration Tool

A helpful tool for calibrating a Kinect v2 + projector setup as used for several augmented reality applications.

## Dependencies:
1. OpenCV 3.x
2. libfreenect2
3. Eigen 3
4. Ceres
5. Qt5

## Build Instructions:
This QT project can simply be opened and built with any current version of QtCreator. However, make sure to adjust the paths in the .pro file to your local setup. A cmake configuration might be provided in the future.

## Usage:
0. The basic calibration procedure is the usual: Collect correspondences using a calibration object and optimize the parameters of the assumed theoretical setup to fit these observations. In order to generate correspondences between the RGB projector and the Kinect's depth camera, we project a crosshair with the projector, manually place a target disc at a matching position and detect this disc in the Kinect's depth image. The target disc as shown in the image below can be printed and cut out using [this template](https://gitlab.com/ungetym/kinect_projector_calib/blob/master/imgs/target.pdf).
![Target disc](https://gitlab.com/ungetym/kinect_projector_calib/raw/master/imgs/howto_target.png "Target disc")
1. After preparing the target disc, start the tool. As shown in the image below, the initial window shows only very few clickable elements in order to give the user an intuitive idea of the workflow.
![GUI Overview](https://gitlab.com/ungetym/kinect_projector_calib/raw/master/imgs/howto_1_init.png "GUI Overview")
Note: The IO menu contains the necessary elements to load and save calibration as well as correspondence data. For this tutorial, however, I will assume that you are starting this tool for the first time and therefore have no previous data.
2. Start the Kinect by clicking the "Start" button. The sliders can be used to adjust the captured Kinects depth range. After successfully starting the Kinect v2 the depth image stream will be shown in the Image View.
![GUI Overview](https://gitlab.com/ungetym/kinect_projector_calib/raw/master/imgs/howto_2_targets.png "GUI Overview")
3. Open the projector window by pressing the "Open" button. A black fullscreen window will be opened on one of the available output devices (monitor or projector). If the window opened on the wrong screen, you can simply move it to the correct one by repeatedly pressing the "Switch Screen" button.
After opening this window, the Target Setup will be available as shown in the screenshot above.
4. With those options you can select the strategy for showing the crosshair at different positions. The "Preconfigure List" will automatically calculate an equal distribution of the given number of positions w.r.t. the projectors resolution. (Note: The number of targets set in the GUI might automatically be increased in order to guarantee that all areas of the projector image are covered.) The "Manual" option allows you to specify every crosshair position yourself or (if the checkbox "Random Position" is checked) to generate the positions randomly. 
5. If you are satisfied with your selection, click on "Show Next Target Image" in order to show the first crosshair position.
![GUI Overview](https://gitlab.com/ungetym/kinect_projector_calib/raw/master/imgs/howto_3_capture.png "GUI Overview")
6. Place the target disc so that the crosshair is located in its center as shown in the images below.
![Crosshair](https://gitlab.com/ungetym/kinect_projector_calib/raw/master/imgs/howto_crosshair.png "Crosshair")
![Disc positioning](https://gitlab.com/ungetym/kinect_projector_calib/raw/master/imgs/howto_placetarget.png "Disc positioning")
7. Press the button "Capture and Detect". Using a combination of Hough circles and a checkboard corner detection, the position of the disc is then automatically detected and the result is shown in the "Image View".
![GUI Overview](https://gitlab.com/ungetym/kinect_projector_calib/raw/master/imgs/howto_4_detected.png "GUI Overview")
You now have the options to accept or reject the result using the respective buttons. Furthermore you can tell the tool to retry the detection, e.g. if you have the impression that the failed detection might be the result of noise. Note: The automatic detection is more reliable if the contrast between background and disc is larger. Therefore it might help to set the maximum depth distance (via the slider) to a lower value, so that the table/background is set to depth 0.0.
8. If the checkbox "Show Next Target On Accept" is checked, after every accept, a new crosshair position is shown and you can repeat the steps 5 and 6.
9. After collecting multiple correspondences you can start the optimization by clicking "Start Optimization".
10. In order to test the quality of the calibration result I added a validation mode, which can be (de-)activated using the "Toggle Validation Mode" button. In this mode the depth image stream is directly given to the disc detector that tries to detect target discs. The resulting Kinect pixel coordinates and corresponding depths are then projected to the projector image plane using the calibrated parameters and shown via the crosshair.
11. If you are satisfied with the result, you can save the calibration as well detected correspondences using the IO menu.

Notes: Make sure, not to place the target disc at the same depth distance for every correspondence. Otherwise the 3D points are placed on a plane which will almost certainly lead to bad calibration results due to matrix degeneration during the optimization.

Technical note: The radial distortion for both, the projector as well as the Kinect v2, is currently ignored since it is assumed to be corrected by the respective drivers. However, the correctness of this assumption depends on the specific setup and therefore I plan to include the distortion handling in the near future.
